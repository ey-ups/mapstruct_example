package com.example.patika.mapper;

import com.example.patika.dto.CourseDto;
import com.example.patika.model.Course;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;


@Mapper(componentModel = "spring")
public interface CourseMapper {

    CourseMapper INSTANCE = Mappers.getMapper(CourseMapper.class);

    @Mapping(source = "category_id",target = "category")
    Course toModel(CourseDto courseDto);

    @InheritConfiguration
    void updataModel(CourseDto courseDto, @MappingTarget Course course);

    @Mapping(source = "category", target = "category_id")
    CourseDto toDto(Course course);

    List<CourseDto> toDtoList(List<Course> courses);

    List<Course> toModelList(List<CourseDto>courseDtos);

}

