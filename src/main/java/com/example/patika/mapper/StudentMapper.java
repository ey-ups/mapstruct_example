package com.example.patika.mapper;

import com.example.patika.dto.StudentDto;
import com.example.patika.model.Student;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;


@Mapper(componentModel = "spring")
public interface StudentMapper {

    StudentMapper INSTANCE = Mappers.getMapper(StudentMapper.class);

    @Mapping(source = "student_no",target = "no")
    Student toModel(StudentDto studentDto);

    @InheritConfiguration
    void updataModel(StudentDto studentDto, @MappingTarget Student student);

    @Mapping(source = "no", target = "student_no")
    StudentDto toDto(Student student);

    List<StudentDto> toDtoList(List<Student> students);

    List<Student> toModelList(List<StudentDto>studentDtos);

}

