package com.example.patika.service;

import com.example.patika.dto.CourseDto;
import com.example.patika.mapper.CourseMapper;
import com.example.patika.model.Course;
import com.example.patika.repository.CourseRepository;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CourseService {


    private final CourseRepository courseRepository;

    public CourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }


    public List<CourseDto> getCourses() {
        return CourseMapper.INSTANCE.toDtoList(courseRepository.findAll());
    }


    public Course getCourse(Long id) throws NotFoundException {
        Optional<Course> courseOptional = courseRepository.findById(id);

        return courseOptional.orElseThrow(() -> new NotFoundException("Course not found"));
    }


    public Long createCourse(CourseDto courseDto) {
         return  courseRepository.save(CourseMapper.INSTANCE.toModel(courseDto)).getId();
    }

    public String updateCourse(CourseDto courseDto, Long id) throws NotFoundException {
        Optional<Course> courseOptional = courseRepository.findById(id);
        Course course = courseOptional.orElseThrow(() -> new NotFoundException("course was not found"));
        CourseMapper.INSTANCE.updataModel(courseDto, course);
        courseRepository.saveAndFlush(course);
        return id + " was changed";
    }

    public String deleteCourse(Long id) throws NotFoundException {
        Optional<Course> courseOptional = courseRepository.findById(id);
        Course s = courseOptional.orElseThrow(() -> new NotFoundException("course was not found"));
        courseRepository.delete(s);
        return id + " was deleted";
    }

    public List<Course> getCoursesWithId() {
        return courseRepository.findAll();
    }
}
