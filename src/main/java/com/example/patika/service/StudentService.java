package com.example.patika.service;


import com.example.patika.dto.StudentDto;
import com.example.patika.mapper.StudentMapper;
import com.example.patika.model.Student;
import com.example.patika.repository.StudentRepository;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService {

    private final StudentRepository studentRepository;

    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public List<StudentDto> getStudents() {
        return StudentMapper.INSTANCE.toDtoList(studentRepository.findAll());
    }


    public StudentDto getStudent(Long id) throws NotFoundException {
        Optional<Student> studentOptional = studentRepository.findById(id);
        Student student = studentOptional.orElseThrow(() -> new NotFoundException("Student not found"));
        return StudentMapper.INSTANCE.toDto(student);
    }


    public Long createStudent(StudentDto studentDto) {
        return studentRepository.save(StudentMapper.INSTANCE.toModel(studentDto)).getId();
    }

    public String updateStudent(StudentDto studentDto, Long id) throws NotFoundException {
        Optional<Student> studentOptional = studentRepository.findById(id);
        Student student = studentOptional.orElseThrow(() -> new NotFoundException("student was not found"));
        StudentMapper.INSTANCE.updataModel(studentDto,student);
        studentRepository.saveAndFlush(student);
        return id + " was changed";
    }

    public String deleteStudent(Long id) throws NotFoundException {
        Optional<Student> studentOptional = studentRepository.findById(id);
        Student s = studentOptional.orElseThrow(() -> new NotFoundException("student was not found"));
        studentRepository.delete(s);
        return id + " was deleted";
    }

}
