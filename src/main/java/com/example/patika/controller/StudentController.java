package com.example.patika.controller;

import com.example.patika.dto.StudentDto;
import com.example.patika.service.StudentService;
import javassist.NotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {

    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping
    public List<StudentDto> getStudents() {
        return studentService.getStudents();
    }

    @GetMapping("{id}")
    @ResponseBody
    public StudentDto getStudent(@PathVariable Long id) throws NotFoundException {
        return studentService.getStudent(id);
    }

    @PostMapping
    public Long createStudent(@RequestBody StudentDto studentDto) {
        return studentService.createStudent(studentDto);

    }


    @PutMapping("{id}")
    public String updateStudent(@RequestBody StudentDto studentDto, @PathVariable Long id) throws NotFoundException {
        return studentService.updateStudent(studentDto, id);

    }

    @DeleteMapping("{id}")
    public String deleteStudent(@PathVariable Long id) throws NotFoundException {
        return studentService.deleteStudent(id);
    }


}
