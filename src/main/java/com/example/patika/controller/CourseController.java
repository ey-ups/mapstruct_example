package com.example.patika.controller;

import com.example.patika.dto.CourseDto;
import com.example.patika.mapper.CourseMapper;
import com.example.patika.model.Course;
import com.example.patika.service.CourseService;
import javassist.NotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/course")
public class CourseController {

    private final CourseService courseService;

    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping("/id")
    public List<Course> getCoursesWithId(){
        return courseService.getCoursesWithId();
    }

    @GetMapping
    public List<CourseDto> getCourses() {
        return courseService.getCourses();
    }

    @GetMapping("{id}")
    @ResponseBody
    public CourseDto getCourse(@PathVariable Long id) throws NotFoundException {
        return CourseMapper.INSTANCE.toDto(courseService.getCourse(id));
    }

    @PostMapping
    public Long createCourse(@RequestBody CourseDto courseDto) {
        return courseService.createCourse(courseDto);

    }


    @PutMapping("{id}")
    public String updateCourse(@RequestBody CourseDto courseDto, @PathVariable Long id) throws NotFoundException {
        return courseService.updateCourse(courseDto,id);

    }

    @DeleteMapping("{id}")
    public String deleteCourse(@PathVariable Long id ) throws NotFoundException {
        return courseService.deleteCourse(id);
    }
}




